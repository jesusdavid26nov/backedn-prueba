const express = require('express');
const morgan = require('morgan');
const cors = require('cors')
const { mongoose } = require('./database');
const app = express();

app.set('port', process.env.PORT || 3000);

app.use(cors())
app.use(morgan('dev'));
app.use(express.json());

app.use('/api/users', require('./routes/users.routes'));

app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
})
