const User = require('../models/user.model');
const userController = {};

userController.getUsers = async (req, res) => {
    try {
        const users = await User.find();
        res.json(users)
    } catch (error) {
        throw error
    }

}

userController.getUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        res.json(user)
    } catch (error) {
        throw error
    }

}

userController.createUser = async (req, res) => {
    try {

        const user = new User(req.body);
        const duplicateEmail = await User.findOne({ email: user.email });
        if (duplicateEmail) {
            return res.json(
                { status: 'duplicate email' }
            );
        }
        const duplicateCardId = await User.findOne({ idCard: user.idCard });
        if (duplicateCardId) {
            return res.json(
                { status: 'duplicate card' }
            );
        }
        await user.save()
        res.json({ status: 'user save' });
    } catch (error) {
        throw error
    }

}

userController.updateUser = async (req, res) => {
    try {
        const { id } = req.params;
        const { name, lastname, email, phone, idCard } = req.body
        const user = {
            name,
            lastname,
            idCard,
            email,
            phone
        }
        await User.findByIdAndUpdate(id, { $set: user })
        res.json({ status: 'user updated' });
    } catch (error) {
        throw error
    }

}

userController.deleteUser = async (req, res) => {
    try {
        await User.findByIdAndRemove(req.params.id);
        res.json({ status: 'user delete' })
    } catch (error) {
        throw error
    }
}

module.exports = userController;